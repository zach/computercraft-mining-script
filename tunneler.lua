--[[
    Project Name: ZaCo Mining Turtle
    Author: Zach Tancrell <codeberg.org/zach>
    Description: This script is designed to automate mining in Minecraft using the ComputerCraft / CC: Tweaked  mod's mining turtles. 
                 It includes features such as fuel checks, automatic torch placement, and handling of lava and water obstacles.
                 It is heavily inspired on the Fallout universe RobCo terminals.

    Date Created: February 21, 2024
    License: GNU General Public License v3 (GPLv3)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

-- Global Variables
-- The amount of fuel for refueling
local FUEL_THRESHOLD = 100

-- amount of fuel to consume on refuel
local REFUEL_AMOUNT = 5

-- Just a sleep timer
local SLEEP_TIME = 5

-- The slots for Fuel, Cobblestone, Torches
local FUEL_SLOT = 1
local COBBLESTONE_SLOT = 2
local TORCH_SLOT = 3

-- Torch placement variables
local TORCH_PLACEMENT_FREQUENCY = 5

-- Keep track of excavation cycles
local cycleCounter = 0

-- track if a torch was just placed
local torchJustPlaced = false

-- Scrollable content
local lines = {
   "         <up / down to scroll>",
   "=======================================",
   "       ZaCo Mining Turtle Terminal",
   "=======================================",
   "             Mine OS v1.0",
   "=======================================",
   "",
   "This Mining Turtle terminal is exclusively for ZaCo employees.",
   "",
   "Unauthorized access is strictly prohibited.",
   "",
   "For assistance, contact your system administrator.",
   "",
   "=======================================",
   "",
   "Root access reserved for authorized ZaCo personnel.",
   "",
   "",
   "ZaCo personnel, kindly press 'enter' to initiate secure login.",
   "======================================="
}

-- variables for displaying the lines of character
local startIndex = 1
local endIndex = math.min(#lines, 4)  -- Display 4 lines at a time

-- Get size of terminal
local width, height = term.getSize()

for i, line in ipairs(lines) do
   -- calc pos to center the text
   local x = math.floor((width - #lines) / 2)
   local y = i

   term.setCursorPos(x, y)

   print(line)
end

-- Displays the lines of text in the above variable 'lines'
function displayContent()
   term.setBackgroundColor(colors.black)
   term.setTextColor(colors.lime)
   for i = startIndex, endIndex do
      print(lines[i])
   end
   term.setTextColor(colors.white)
end

-- Handles the screen scroll up
function scrollUp()
   if startIndex > 1 then
      startIndex = startIndex - 1
      endIndex = endIndex - 1
   end
end

-- Handles the screen scroll down
function scrollDown()
   if endIndex < #lines then
      startIndex = startIndex + 1
      endIndex = endIndex + 1
   end
end

-- just prings a message with a cool banner
function printHeader(header)
   term.setBackgroundColor(colors.black)
   term.setTextColor(colors.lime)
   
   print("=======================================")
   print(header)
   print("=======================================")
end

-- Prints error message in red
function printError(message)
   term.setBackgroundColor(colors.red)
   term.setTextColor(colors.white)

   print(message)

   term.setTextColor(colors.lime)  -- Set the text color to lime for subsequent messages
end

-- prints message with arrow
function printMessage(message)
   term.setBackgroundColor(colors.black)
   term.setTextColor(colors.lime)

   print("=> " .. message)
end

-- Display a message on the Mining Turtle
function displayMessage(message)
   term.clear()
   term.setCursorPos(1, 1)
   print(message)
end

-- initiate function (kinda for looks; tells user where to put fuel, blocks, torches)
function initiate()
   -- Display instructions for arranging the inventory
   printHeader("            ZaCo Init Process")
   printMessage("Organize turtle inventory: \n - fuel\n - cobblestone\n - torches\nin Slots 1, 2, and 3, respectively.\n")
   
   -- Additional message for user confirmation
   print("\nFor ZaCo employees, kindly initiate the process by entering 'start'.")

   -- Wait for user confirmation
   local confirmation, key = read() and os.pullEvent("key")
   if confirmation:lower() ~= "start" and key == "enter" then
      printError("Error: Mining operating cancelled!")
   end

   -- Clear the screen and display a message while initiating the turtle
   term.clear()
   displayMessage("Initiating [TURTLE::MINE_TUNN] protocol. Please wait.")

   -- Display a message after the turtle is initiated
   displayMessage("[TURTLE::MINE_TUNN] Initiated. Beginning protocol...")
end

function checkBelowTurtle()
   local success, data = turtle.inspectDown()

   if success then
      if data.name == "minecraft:lava" or data.name == "minecraft:water" then
         turtle.select(COBBLESTONE_SLOT)
         turtle.placeDown()
      else
         return
      end
   else
      -- Only place a block if a torch was not placed
      if not torchJustPlaced then
         turtle.select(COBBLESTONE_SLOT)
         turtle.placeDown()
      end
   end
end

-- Prints out fuel status, and checks if fuel is below 500 and refuels.
function checkFuel()
   printHeader("ZaCo Mining Turtle Fuel Check")
   local fuelLevel = turtle.getFuelLevel()
   printMessage("Fuel Level: " .. fuelLevel)

   if fuelLevel < FUEL_THRESHOLD then
      turtle.select(FUEL_SLOT)
      if turtle.refuel(REFUEL_AMOUNT) then
         printMessage("Refueled Turtle. Resuming excavation.")
      else
         printMessage("Error: No fuel available. Please add more fuel.")
         printMessage("Program will pause and wait for more fuel.")
         printMessage("All Hail, ZaCo!")

         -- wait for turtle to be refuel, continue
         while fuelLevel < FUEL_THRESHOLD and turtle.getItemCount(FUEL_SLOT) == 0 do
            os.sleep(SLEEP_TIME)
         end

         turtle.select(FUEL_SLOT)
         turtle.refuel(REFUEL_AMOUNT)

         printMessage("Refueled Turtle. Resuming excavation.")
      end
   end
end

function moveTurtle(direction) 
   if direction == "forward" then
      turtle.forward()
   elseif  direction == "back" then
      turtle.back()
   elseif direction =="up" then
      turtle.up()
   elseif direction == "down" then
      turtle.down()
   else
      printMessage("Error: Invalid direction '" .. direction .. "'")
   end
end

function turnTurtle(direction)
   if direction == "left" then
      turtle.turnLeft()
   elseif direction == "right" then
      turtle.turnRight()
   end
end

-- Function that handles the 3x3 mining
function hammer()
   -- x x x
   -- x x x
   -- x x x
--#region First layer
   -- Digs block in front, and moves forward 1 block
   turtle.dig()
   moveTurtle("forward")

   checkBelowTurtle() -- Checks below the turtle for lava or water and places a block

   -- Looks Left, and Digs 1 block
   turnTurtle("left")
   turtle.dig()

   -- Looks right twice, digs 1 block right
   turnTurtle("right")
   turnTurtle("right")
   turtle.dig()

   -- Centers itself, Digs 1 up, moves up
   turnTurtle("left")
   turtle.digUp()
   turtle.up()
--#endregion

--#region Second layer
   -- Looks left, digs 1 block
   turnTurtle("left")
   turtle.dig()

   -- Looks right twice, digs 1 block
   turnTurtle("right")
   turnTurtle("right")
   turtle.dig()

   -- Centers itself, digs 1 up and moves up
   turnTurtle("left")
   turtle.digUp()
   moveTurtle("up")
--#endregion

--region Third layer
   -- Looks Left, Digs 1 block
   turnTurtle("left")
   turtle.dig()

   -- Looks Right, Digs 1 block
   turnTurtle("right")
   turnTurtle("right")
   turtle.dig()

   -- Centers itself, Moves down
   turnTurtle("left")
   moveTurtle("down")
   turtle.down()
   turtle.down()
--#endregion
end

-- Function to place torches

-- TODO: FIX THE FUCKING TORCH PLACING
function placeTorch()
   turtle.select(TORCH_SLOT)
   if turtle.placeDown() then  -- If the torch is successfully placed
      torchJustPlaced = true
      moveTurtle("down")
   end
end

function excavation()
   cycleCounter = cycleCounter + 1 -- increment each time excavation() is called
   printMessage("internal torch counter :: " .. cycleCounter)

   -- if cycle is greater than or equal to torch_placement_frequency, place torch.
   if cycleCounter >= TORCH_PLACEMENT_FREQUENCY then
      placeTorch() -- place the torch      
      cycleCounter = 0
   end
   hammer()
   torchJustPlaced = false  -- Reset the torchJustPlaced variable after each excavation cycle
end

function start()
   initiate()
   while true do
      checkBelowTurtle() -- Checks below the turtle for lava or water and places a block
      checkFuel()
      excavation()
   end
end

-- Main program
function mainLoop()
   while true do
      term.clear()
      displayContent()

      local event, key = os.pullEvent("key")
      if event == "key" then
         if key == keys.up then
            scrollUp()
         elseif key == keys.down then
            scrollDown()
         elseif key == keys.q then
            break
         elseif key == keys.enter then
            term.clear()
            start()
            break
         end
      end
   end
end

-- Start the main loop
mainLoop()